#==========================================#
# Elaborado por: Eduard F Martinez-Gonzalez
# Ultima modificacion: 22-03-2022
# R version 4.1.1 (2021-08-10)
#==========================================#

# initial configuration
rm(list=ls())
require(pacman) # llamar pacman
p_load(tidyverse,rio,sf,osmdata,sp,viridis,leaflet,png,grid,
       ncdf4,  # Leer ncdf files
       raster, # Manipular datos raster
       stars)  # Manipular datos raster

# raster: raster(), stack() 
# stars: read_stars()
# png, grid: leer graficos

#=================#
# [1.] Motivacion #
#=================#

# Night lights and economic growth
dev.off()
grid.raster(readPNG("input/pics/Indonesia.png")) # Tomado de: Measuring Economic Growth from Outer Space
grid.raster(readPNG("input/pics/Rwanda.png"))    # Tomado de: Measuring Economic Growth from Outer Space
browseURL("https://www.aeaweb.org/articles?id=10.1257/aer.102.2.994") # Ir a: Measuring Economic Growth from Outer Space

# Night lights and Covid-19
dev.off()
grid.raster(readPNG("input/pics/covid.png")) 
browseURL("https://economia.uniandes.edu.co/sites/default/files/observatorio/Resultados-luminosidad.pdf") # Ir a: COVID19 y actividad económica: demanda de energía y luminosidad ante la emergencia

# Examining the economic impact of COVID-19 in India through daily electricity consumption and nighttime light intensity
browseURL("https://www.sciencedirect.com/science/article/pii/S0305750X20304149?casa_token=VjcoqRnPQo8AAAAA:DXanp0b9C9w6BvR9C3NF0WfEvWO7uB_DvGBBxC8b8d59l2QVcAdRLGKLf4e-UhGr6HRECXNvxw")

# Measuring the size and growth of cities using nighttime light
browseURL("https://www.sciencedirect.com/science/article/pii/S0094119020300255?casa_token=cMiibzNrYTEAAAAA:YqwYomGC9QgC81nDaYQmQ_z6Fak1WQgtW6JwobSzTOQCfVsqCzprUGK77XEphlO60A0-eWpPCg") 

# Russia-Ukraine War as Seen by DNB
browseURL("https://eogdata.mines.edu/products/special_topics/russia_ukraine_war.html")

#==============#
# [2.] Sources # 
#==============#

#=== Night lights ===#

# DMSPL (1992-2013)
browseURL("https://www.ngdc.noaa.gov/eog/dmsp/downloadV4composites.html") # Anuales 

# VIIRS (2012-Currently)
browseURL("https://eogdata.mines.edu/products/vnl/") # Link del proyecto
browseURL("https://eogdata.mines.edu/nighttime_light/monthly/v10/") # Mensuales
browseURL("https://ngdc.noaa.gov/eog/viirs/download_ut_mos.html") # Diarios

# Harmonized (1992-2018)
browseURL("https://figshare.com/articles/dataset/Harmonization_of_DMSP_and_VIIRS_nighttime_light_data_from_1992-2018_at_the_global_scale/9828827/2") # Anuales 

# Download and tidy data
browseURL("https://gitlab.com/main_data/night_lights/-/tree/main/1_download/scr") # Gitlab

#=== Land cover ===#

# Sentinel - Copernicus
browseURL("https://lcviewer.vito.be/about#available-maps") # about sentinel
browseURL("https://land.copernicus.eu/global/products/lc") # pagina
browseURL("https://zenodo.org/record/4723921#.YZFYNS-B1hE") # Dictionary

#=== NASA ===#

# Temperature, rainfall,...
browseURL("https://disc.gsfc.nasa.gov")

#==================================#
# [3.] Introduccion a datos raster #
#==================================#

##=== Acerca de los raster ===#

## qué es un raster?
dev.off()
grid.raster(readPNG("input/pics/raster.png")) # fuente: https://www.neonscience.org

## resolucion
dev.off()
grid.raster(readPNG("input/pics/rasterize.png")) # poner fuente

## bandas de un raster
dev.off()
grid.raster(readPNG("input/pics/rgb_raster.png")) # Imagen tomada de https://www.neonscience.org

##=== Leer un raster ===#

## importar raster de luces
luces_r = raster('input/rasters/night_light_202003.tif')
luces_r
luces_s = read_stars("input/rasters/night_light_202003.tif")
luces_s
0.00416667*111000 # resolucion

## geometria
st_bbox(luces_s)
st_crs(luces_s)
st_dimensions(luces_s)

## atributos
names(luces_s)
names(luces_s) = "date_202003"

## valores del raster
luces_s[[1]] %>% max(na.rm = T)
luces_s[[1]] %>% min(na.rm = T)
luces_s[[1]] %>% as.vector() %>% summary() 
luces_s[[1]][is.na(luces_s[[1]])==T] # Reemplazar NA's
luces_s[[1]][2000:2010,2000:2010]
luces_s[[1]][2000:2010,2000:2010] %>% table() # Sustraer una parte de la matriz

## puedo reproyectar un raster?
st_crs(luces_s)
luces_new_crs = st_transform(luces_s,crs=4126)
luces_s[[1]][2000:2010,2000:2010] # no se alteran las geometrias
luces_new_crs[[1]][2000:2010,2000:2010] # no se alteran las geometrias

## Plot data
plot(luces_s)

##=== Hacer clip a un raster ===#

## Download boundary
medellin = opq(bbox = getbb("Medellín Colombia")) %>%
           add_osm_feature(key = "boundary", value = "administrative") %>% osmdata_sf()

medellin = medellin$osm_multipolygons %>% 
           dplyr::select(osm_id,name,admin_level,boundary)
medellin
medellin = medellin %>% 
           dplyr::filter(admin_level==7) 
medellin
medellin = medellin %>%  
           dplyr::filter(osm_id==3541280)
medellin
#export(medellin,"output/medellin.rds")
#medellin = import("output/medellin.rds")

ggplot() + geom_sf(data=medellin , col="red") + theme_bw() 
leaflet() %>%
addTiles() %>%
addPolygons(data=medellin)

## cliping
luces_medellin_1 = st_crop(x = luces_s , y = medellin) # crop luces de Colombia con polygono de Medellin

## Plot data
ggplot() + geom_stars(data=luces_medellin_1 , aes(y=y,x=x,fill=date_202003)) + # plot raster
scale_fill_viridis(option="A" , na.value='white') +
geom_sf(data=medellin , fill=NA , col="green") + theme_bw() 

#==================================================#
# [4.] Aplicación: Actividad económica en Medellín #
#==================================================#

##=== Importar datos ===#

## load data
luces_medellin_0 = read_stars("input/rasters/night_light_202002.tif") %>% 
                   st_crop(.,medellin)
names(luces_medellin_0) = "date_202002"

ggplot() + geom_stars(data=luces_medellin_0 , aes(y=y,x=x,fill=date_202002)) + # plot raster
scale_fill_viridis(option="A" , na.value='white') +
geom_sf(data=medellin , fill=NA , col="green") + theme_bw() 

##=== Stack rasters ===#

## Unir los daster de 202002 y 202003
luces_medellin = c(luces_medellin_0,luces_medellin_1)
luces_medellin

##=== Get building data ===#

## get OSM-data
house = opq(bbox = getbb("Medellín Colombia")) %>% add_osm_feature(key = "building", value = "house") %>% 
        osmdata_sf() %>% .$osm_polygons %>% .[,c("osm_id")] %>% mutate(building="house")

commercial = opq(bbox = getbb("Medellín Colombia")) %>% add_osm_feature(key = "building", value = "commercial") %>% 
             osmdata_sf() %>%  .$osm_polygons %>% .[,c("osm_id")]  %>% mutate(building="commercial")

hotel = opq(bbox = getbb("Medellín Colombia")) %>% add_osm_feature(key = "building", value = "hotel") %>% 
        osmdata_sf() %>%  .$osm_polygons %>% .[,c("osm_id")]  %>% mutate(building="hotel")

construction = opq(bbox = getbb("Medellín Colombia")) %>% add_osm_feature(key = "building", value = "construction") %>% 
               osmdata_sf() %>%  .$osm_polygons %>% .[,c("osm_id")]  %>% mutate(building="construction")

building = rbind(house,commercial) %>% rbind(.,hotel) %>% rbind(.,construction) %>% .[medellin,]
#export(building,"input/building.rds")

## import data
building = import("input/building.rds")

building %>% head()

ggplot() + geom_sf(data = building , aes(fill=building) , col=NA)

##=== Raster a datos vectoriales ===#

## Opcion 1 (puede tardar un poco)
opcion_1 = st_extract(x = luces_medellin, at = building , FUN = mean) %>% st_as_sf()
opcion_1 %>% head()

## Opción 2
luces_medellin_sf = st_as_sf(x = luces_medellin, as_points = T, na.rm = T) # raster to sf (points)
luces_medellin_sf

luces_medellin_sf2 = st_as_sf(x = luces_medellin, as_points = F, na.rm = T) # raster to sf (polygons)
luces_medellin_sf2

## Plot data
ggplot() + 
geom_sf(data = luces_medellin_sf , aes(fill=date_202002))  + 
scale_fill_viridis(option="A" , na.value='white') +
geom_sf(data=medellin , fill=NA , col="green") + theme_bw() 

ggplot() + 
geom_sf(data = luces_medellin_sf2 , aes(fill=date_202002),col=NA)  + 
scale_fill_viridis(option="A" , na.value='white') +
geom_sf(data=medellin , fill=NA , col="green") + theme_bw()  + 
geom_sf(data = building , col=NA , fill="white")

##=== Asignar el valor del pixel al los building ===#
luces_building = st_join(x=building , y=luces_medellin_sf2 , largest=F) # join layers

a = subset(luces_building,osm_id==142638995)
leaflet() %>%
addTiles() %>% addPolygons(data=a,col="blue")
        
## variacion promedio 
df = luces_building

st_geometry(df) = NULL

df %>% 
group_by(building) %>% 
summarise(pre=mean(date_202002,na.rm=T) , 
          post=mean(date_202003,na.rm=T)) %>%
mutate(ratio=1-pre/post)

#================================================#
# [5.] Aplicación: Cambio de cobertura de suelo? #
#================================================#

# Load data
land_cover = c(read_stars("input/rasters/discreta_2015.tif"),
               read_stars("input/rasters/discreta_2019.tif"))
land_cover
0.000992063*111000 # resolucion

# cliping raster
medellin_10k = st_buffer(medellin,dist=0.1) %>% st_difference(medellin)
land_medellin_10k = st_crop(land_cover,medellin_10k)
land_medellin = st_crop(land_cover,medellin)

# view data
plot(land_medellin_10k)

# raster to sf
land_10k_sf = st_as_sf(x = land_medellin_10k, as_points = F, na.rm = T) 

# cuantos pixeles se urbanizaron?
land_10k_sf = land_10k_sf %>% 
              rename(c_2015=discreta_2015.tif,c_2019=discreta_2019.tif)
land_10k_sf = land_10k_sf %>% 
              mutate(dife = as.numeric(c_2015)-as.numeric(c_2019))

#====================#
# [6.] Download Data #
#====================#

## getSpatialData
browseURL("https://github.com/16EAGLE/getSpatialData")
p_load(devtools)
devtools::install_github("16EAGLE/getSpatialData")

## log in: EARTHDATA
browseURL("https://urs.earthdata.nasa.gov")    
login_earthdata(username = "ef.martinezg")

## log in: Copernicus Open Access Hub
browseURL("https://scihub.copernicus.eu/dhus/#/self-registration")    
login_CopHub(username = "ef.martinezg")

## log in: 
browseURL("https://ers.cr.usgs.gov/register/")
login_USGS("ef.martinezg")

# Print the status of all services:
services()

## ver datos disponibles
get_products()

# establecer el area de interes
medellin_sp = as_Spatial(medellin)
set_aoi(medellin_sp)

## Obtener las url de los datos
records = get_records(time_range = c("2021-05-15", "2021-05-20"),
                       products = c("sentinel-2"))

## Verificar disponibilidad de los raster
records = check_availability(records, verbose = TRUE)
records = subset(records,download_available==T & level=="Level-2A")

## Visualizar datos
view_records(records)
records = get_previews(records,dir_out = "output") 
sentinel = stack("output/sentinel-2/S2A_MSIL2A_20210516T153621_N0300_R068_T18NVM_20210516T194809_preview.tif")
plot(sentinel , col=magma(18))
plotRGB(sentinel,r = 1, g = 2, b = 3) 

## Calidad de los raster (cobertura de nubes)
records = calc_cloudcov(records,dir_out = "output") 

## Descargar los raster
records <- get_data(records,dir_out = "output")

